#include <stdio.h>

int main(void) {
    printf("Opening ciphertext...\n");
    FILE* file = fopen("./test/ciphertext.txt", "r");

    int c;
    while ((c = getc(file)) != EOF)
        putchar(c);

    printf("Closing...\n");
    fclose(file);

    printf("Opening plaintext...\n");
    file = fopen("./test/plaintext.txt", "r");

    while ((c = getc(file)) != EOF)
        putchar(c);

    printf("Closing...\n");
    fclose(file);

    return 0;
}