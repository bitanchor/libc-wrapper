'''
@blame R. Matt McCann <mccann.matt@gmail.com>
@brief A collection of helper functions for the build process
@copyright &copy; 2017 Evil Corp.
'''

import os
import sys
from datetime import datetime
from time import time

from glob import glob
from SCons.Script import Dir, GetOption, VariantDir
from subprocess import Popen

from misc.terminal import StdOut


# This is injected by the SCons build runner
env = None


def CleanCMake(target, source, env, clear_log=True):
    '''
    Runs the cleanup of the cmake if needed.

    @param target Target to be cleaned
    @param source Directory where the Makefile lives
    @param env Environment build configuration
    '''
    CleanConfigureMake(target, source, env, do_uninstall=False, in_build_dir=True)


def CleanConfigureMakeNoUninstall(target, source, env):
    CleanConfigureMake(target, source, env, do_uninstall=False)


def CleanConfigureMake(target, source, env, clear_log=True, do_uninstall=True, in_build_dir=False):
    '''
    Runs the cleanup of the configure make if needed.

    @param target Target to be cleaned
    @param source Directory where the Makefile lives
    @param env Environment build configuration
    '''
    target_name = target[0]

    orig_dir = '.'
    base_dir = '.'

    if in_build_dir:
        base_dir = './build'

    # If this is a cleanup command
    if GetOption('clean') and (len(sys.argv) == 2 or target_name in sys.argv):
        # Calculate the log file location
        log_file = '%s/logs/%s.clean' % (Dir('#').abspath, target_name)
        if clear_log:
            os.system('rm -f %s' % log_file)

        print StdOut.OKBLUE, "CLEANING...", StdOut.ENDC, target_name

        try:
            if len(glob('./install/*')) == 0 and\
                    len(glob('./build/*')) == 0 and not os.path.exists('config.status'):
                print StdOut.OKGREEN, "ALREADY CLEAN!", StdOut.ENDC, target_name

            else:
                if do_uninstall:
                    run(orig_dir, base_dir, 'make uninstall', log_file)
                run(orig_dir, '.', 'rm install/* -rf', log_file)
                try:
                    run(orig_dir, base_dir, 'make clean', log_file)
                except:
                    pass
                run(orig_dir, '.', 'rm build/* -rf', log_file)
                if os.path.exists('config.status'):
                    run(orig_dir, base_dir, 'rm config.status', log_file)

                for raw_target_lib in target[1:]:
                    target_lib = raw_target_lib[raw_target_lib.rfind("/")+1:]
                    path = Dir('#').abspath

                    if path.endswith('build'):
                        path += "/../install"
                    else:
                        path += "/install"

                    if 'lib' in target_lib:
                        run(orig_dir, base_dir, 'rm -f %s/lib/%s' % (path, target_lib), log_file)
                    else:
                        run(orig_dir, base_dir, 'rm -f %s/bin/%s' % (path, target_lib), log_file)

                print StdOut.OKGREEN, "CLEANED!   ", StdOut.ENDC, target_name

        except Exception:
            print StdOut.FAIL, "FAILED!    ", target_name, StdOut.ENDC
            print "\tSee %s for details" % log_file
            raise


def CMake(target, source, env, clear_log=True, custom_cmake_command=None):
    '''
    Uses cmake to build to the target.

    @param target Target program to build
    @param source Directory where the cmake lives
    @param env Environment build configuration
    '''
    orig_dir = Dir('#').abspath

    # Calculate the log file location
    target_name = target[0].get_abspath()
    target_name = target_name[target_name.rfind("/")+1:]
    log_file = '%s/logs/%s.build' % (Dir('#').abspath, target_name)
    if clear_log:
        os.system('rm -f %s' % log_file)

    print StdOut.OKBLUE, "BUILDING... ", StdOut.ENDC, target_name

    # Calculate the absolute path of the source folder
    base_dir = source[0].get_abspath()
    base_dir = base_dir[:base_dir.rfind("/")]
    base_dir = '%s/build' % base_dir

    # Generate the Makefile
    cmake_command = 'cmake -DCMAKE_INSTALL_PREFIX:PATH=../install ..'
    if custom_cmake_command:
        cmake_command = custom_cmake_command
    run(orig_dir, base_dir, cmake_command, log_file)

    # Run the Makefile
    source[0] = source[0].File('./build/Makefile')
    ConfigureMake(
        target, source, env, clear_log=False, do_configure=False, do_install=True, show_building=False)

    return 0


def collect_sources(path):
    '''
    @params path Path to search
    @returns List of source files in the folder.
    '''
    c_sources = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.c'))]
    cc_sources = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.cc'))]
    cpp_sources = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.cpp'))]
    assembly_sources = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.s'))]

    return c_sources + cc_sources + cpp_sources + assembly_sources


def configure_scons_library(target, source, env, is_shared=False):
    ''' Configures the SCons Library target so it plays nice with our build system. '''
    target_base = target[:target.rfind('/')+1]
    target_name = target[target.rfind('/')+1:]

    # Configure the build directory
    build_dir = '#build/%s_lib' % target_name
    VariantDir(build_dir, '.')

    # Add the path prefix to the source files
    for iter in range(len(source)):
        source[iter] = build_dir + "/" + source[iter]

    # Set up the appropriately named target
    if is_shared:
        target_lib = env.SharedLibrary(target='%s/%s' % (target_base, target_name), source=source)
    else:
        target_lib = env.Library(target='%s/%s' % (target_base, target_name), source=source)

    env.Alias(target_name, target_lib)


def configure_scons_program(target, source, env):
    ''' Configures the SCons Program target so it plays nice with our build system. '''
    target_base = target[:target.rfind('/')+1]
    target_name = target[target.rfind('/')+1:]

    # Configure the build directory
    build_dir = '#build/%s_program' % target_name
    VariantDir(build_dir, '.')

    # Add the path prefix to the source files
    for iter in range(len(source)):
        source[iter] = build_dir + "/" + source[iter]

    # Set up the appropriately named target
    target_lib = env.Program(target='%s/%s' % (target_base, target_name), source=source)
    env.Alias(target_name, target_lib)


def configure_scons_shared_library(target, source, env):
    ''' Configures the SCons SharedLibrary target. '''
    configure_scons_library(target, source, env, is_shared=True)


def get_environment():
    ''' @returns The appropriate environment. '''
    return env.Clone()


def MakeNoInstall(target, source, env, clear_log=True, show_building=True):
    ''' TODO '''
    ConfigureMake(target, source, env, clear_log, False, False, show_building)


def ConfigureMake(target, source, env, clear_log=True, do_configure=True, do_install=True, show_building=True, custom_configure_args='', custom_make_command=None):
    '''
    Run configure as necessary and then run make.

    @param target Target program to build
    @param source Directory where the Makefile lives
    @param env Environment build configuration
    '''
    orig_dir = Dir('#').abspath

    # Calculate the absolute path of the source folder
    base_dir = source[0].get_abspath()
    base_dir = base_dir[:base_dir.rfind("/")]

    # Calculate the log file location
    target_name = target[0].get_abspath()
    target_name = target_name[target_name.rfind("/")+1:]
    log_file = '%s/logs/%s.build' % (Dir('#').abspath, target_name)
    if clear_log:
        os.system('rm -f %s' % log_file)

    if show_building:
        print StdOut.OKBLUE, "BUILDING... ", StdOut.ENDC, target_name, now()
    start_time = time()

    # Emit the symlinks for the produced targets
    for raw_target_lib in target[1:]:
        target_lib = raw_target_lib.get_abspath()
        target_lib = target_lib[target_lib.rfind("/")+1:]
        path = base_dir

        if path.endswith('build'):
            path += "/../install"
        else:
            path += "/install"

        if 'lib' in target_lib:
            run(orig_dir, Dir('#').abspath + '/lib', 'ln -sf %s/lib/%s' % (path, target_lib), log_file)
        elif not target_lib.endswith('.h'):
            run(orig_dir, Dir('#').abspath + '/bin', 'ln -sf %s/bin/%s' % (path, target_lib), log_file)

    try:
        # If the software hasn't been configured yet
        if not os.path.exists('%s/config.status' % base_dir) and do_configure:
            run(orig_dir, base_dir, './configure --prefix=%s/install %s' % (base_dir, custom_configure_args), log_file)

        # Always run make
        make_command = 'make -j %d' % GetOption('num_jobs')
        if not custom_make_command:
            custom_make_command = ['']
        elif not isinstance(custom_make_command, list):
            custom_make_command = [custom_make_command]
        for cmd in custom_make_command:
            run(orig_dir, base_dir, make_command + ' ' + cmd, log_file)
        if do_install:
            make_command = 'make install -j 8'
            for cmd in custom_make_command:
                run(orig_dir, base_dir, make_command + ' ' + cmd, log_file)

        print StdOut.OKGREEN, "BUILT!      ", StdOut.ENDC, target_name, \
            "(%5.2ds)" % (time() - start_time)
    except Exception:
        print StdOut.FAIL, "FAILED!     ", target_name, StdOut.ENDC
        print "\tSee %s for details" % log_file
        raise

    return 0


def run(orig_dir, target_dir, command, log_file=None):
    ''' Runs the provided command and pipes the output to log file. '''
    exit_code = None
    log = None

    if log_file:
        log = open(log_file, 'a')

    if log_file:
        log.write("======== Running ==========\n")
        log.write("%s\n" % command)
        log.write("===========================\n")
        log.flush()

        command = 'mkdir -p %s && cd %s && %s && cd %s' % (target_dir, target_dir, command, orig_dir)

        proc = Popen(command, stdout=log, stderr=log, shell=True)
        proc.communicate()
        exit_code = proc.wait()

    else:
        proc = Popen(command.split())
        proc.communicate()
        exit_code = proc.wait()

    if exit_code != 0:
        if log_file:
            log.write("======== Failed ==========\n")
            log.write('%s\n' % command)
            log.write("==========================\n\n")
            log.close()

        raise Exception(exit_code)

    if log_file:
        log.write('======== Done ==========\n')
        log.write('%s\n' % command)
        log.write('========================\n\n')
        log.close()

