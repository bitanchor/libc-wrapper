/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Forward declarations for the libc wrapper definitions.
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

#ifndef LIBC_WRAPPER__LIB_C_WRAPPER_H_
#define LIBC_WRAPPER__LIB_C_WRAPPER_H_

extern "C" {

/***********************************************************************************************************************
 * Checks if this is a protected file, and if so, modifies the stat permissions to indicate the file is
 * not accessible if out of context.
 *
 * @param path (in) Path of the file being stat'd
 * @param buf (out) Stat structure previously populated by a stat method call
 **********************************************************************************************************************/
void apply_location_context_to_stat(const char* path, struct stat* buf);

}

#endif // #ifndef LIBC_WRAPPER__LIB_C_WRAPPER_H_