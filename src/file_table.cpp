#include "file_table.h"

#include <cstring>
#include <stdexcept>
#include <unistd.h>

#include "pid.h"

using namespace evil_corp;

FileTable* FileTable::instance_ = NULL;

FileTable::FileTableEntry FileTable::entries_[FileTable::MAX_TRACKED_FILES];

FileTable::FileTable() {
    // Zero out the file table entries
    memset(&entries_, 0, sizeof(FileTable::FileTableEntry) * FileTable::MAX_TRACKED_FILES);
}

FileTable& FileTable::get() {
    // If the singleton does not yet exist, create it
    if (!instance_) instance_ = new FileTable();

    return *instance_;
}

uint16_t FileTable::hash(const size_t file) const {
    // Calculate the XOR'd pointer segments
    const uint16_t upper10 = file >> 20 & 0x3FF;
    const uint16_t mid10 = file >> 10 & 0x3FF;
    const uint16_t lower10 = file & 0x3FF;

    // Calculate the hash value - NOTE(mmccann): Arbitrary, probably crappy hash
    const uint16_t hash = upper10 ^ mid10 ^ lower10;

    // If the record pointed at by the hash value is empty or matches this file, we've found our final index value
    if (entries_[hash].file == file || !entries_[hash].file) return hash;

    // Incrementally traverse the file table until a matching record or an empty record is found
    for (uint16_t iter = hash + 1; iter != hash; iter++) {
        // If we've ran past the bounds of the file table, loop back around
        if (iter >= FileTable::MAX_TRACKED_FILES) iter = 0;

        if (entries_[iter].file == file || !entries_[iter].file) return iter;
    }

    throw std::runtime_error("FileTable appears full! This is likely a bug, and not a naturaly occuring state!");
}

bool FileTable::is_protected_file(const FILE* const file) const {
    bool nothrow = false;
    return is_protected_file((size_t) file, nothrow);
}

bool FileTable::is_protected_file(const int file) const {
    bool nothrow = false;
    return is_protected_file((size_t) file, nothrow);
}

bool FileTable::is_protected_file__throw(const int file) const {
    bool do_throw = true;
    return is_protected_file((size_t) file, do_throw);
}

bool FileTable::is_protected_file(const size_t file, const bool is_throw) const {
    const uint16_t index = hash(file);

    // If this is a properly populated table entry, return the protection status
    if (entries_[index].file) {
        return entries_[index].is_protected_file;
    }

    if (is_throw) {
        throw std::runtime_error("File not registered in FileTable!");
    } else {
        return false;
    }
}

void FileTable::print() const {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();

    fprintf(stderr, "[%d|%d] File Table Contents:\n", pid, tid);
    for (uint16_t iter = 0; iter < MAX_TRACKED_FILES; iter++) {
        if (entries_[iter].file != 0) {
            fprintf(stderr, "[%d|%d] table[%u] = (%lu, %d)\n", pid, tid, iter, entries_[iter].file, entries_[iter].is_protected_file);
        }
    }
    fprintf(stderr, "[%d|%d] =====================\n", pid, tid);
}

void FileTable::track_file(const size_t file, const bool is_protected_file) {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();
    const uint16_t index = hash(file);

    // Save the file to the tracking table
    entries_[index].file = (size_t) file;
    entries_[index].is_protected_file = is_protected_file;

    fprintf(stderr, "[%d|%d] Stashed file %lu at index %u - is protected file is %d\n", pid, tid, file, index, is_protected_file);
    print();
}

void FileTable::track_file(const FILE* const file, const bool is_protected_file) {
    track_file((size_t) file, is_protected_file);
}

void FileTable::track_file(const int file, const bool is_protected_file) {
    track_file((size_t) file, is_protected_file);
}

void FileTable::untrack_file(const FILE* const file) {
    untrack_file((size_t) file);
}

void FileTable::untrack_file(const int file) {
    untrack_file((size_t) file);
}

void FileTable::untrack_file(const size_t file) {
    const uint16_t index = hash(file);

    // Clear the tracking table entry
    entries_[index].file = 0;
}

