#ifndef LIBC_WRAPPER__PID_H_
#define LIBC_WRAPPER__PID_H_

#include <cstdint>

extern "C" {
    uint16_t my_getpid();
    uint16_t my_gettid();
}

#endif