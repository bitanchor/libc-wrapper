#include "location_status.h"

#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <semaphore.h>

using namespace evil_corp;

LocationStatus* LocationStatus::instance_ = NULL;

LocationStatus& LocationStatus::get() {
    // If this singleton has not yet been initialized
    if (instance_ == NULL) {
        // Initialize the singleton
        instance_ = new LocationStatus();
        instance_->init();
    }

    return *instance_;
}

void LocationStatus::init() {
    const int not_in_location_context = 0;

    // Fetch the location service semaphore
    in_location_sem_ = sem_open("human_proof__in_location", O_CREAT | O_RDONLY, S_IRUSR | S_IWUSR, not_in_location_context);
    // TODO: Error checking of some kind?
}

bool LocationStatus::is_within_location_context() const {
    const int is_within_location_context = 1;
    int sem_value;

    sem_getvalue(in_location_sem_, &sem_value);
    // TODO: Error checking of some kind?

    //if (sem_value == is_within_location_context) {
    //    return true;
    //} else {
    //    return false;
    //}
    return true;
}
