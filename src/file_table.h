/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Table used for tracking opened file details
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

#ifndef LIBC_WRAPPER__FILE_TABLE__H_
#define LIBC_WRAPPER__FILE_TABLE__H_

#include <cstdint>
#include <cstdio>

namespace evil_corp {

/**
 * Singleton table used for tracking opened file details for the process.
 */
class FileTable {
public:
    /*******************************************************************************************************************
     * @returns This singleton FileTable instance
     ******************************************************************************************************************/
    static FileTable& get();

    /*******************************************************************************************************************
     * Checks if the FILE pointer refers to a protected file.
     *
     * @param file FILE whose status is to be checked
     * @returns True if file is protected
     ******************************************************************************************************************/
    bool is_protected_file(const FILE* const file) const;
    bool is_protected_file(const int file) const;
    bool is_protected_file__throw(const int file) const;

    /*******************************************************************************************************************
     * Prints the contents of the file table (for debugging purposes)
     ******************************************************************************************************************/
    void print() const;

    /*******************************************************************************************************************
     * Adds the provided file to the file table so it can be tracked.
     *
     * @param file File to be tracked
     * @param is_protected_file Whether or not this is a protected file
     ******************************************************************************************************************/
    void track_file(const FILE* const file, const bool is_protected_file);
    void track_file(const int file, const bool is_protected_file);

    /*******************************************************************************************************************
     * Rmoves the provided file from the file table so that is it no longer tracked.
     *
     * @param file File to be untracked
     ******************************************************************************************************************/
    void untrack_file(const FILE* const file);
    void untrack_file(const int file);

private:
    /*******************************************************************************************************************
     * Maximum number of files that can be tracked in a single process.
     ******************************************************************************************************************/
    static const uint16_t MAX_TRACKED_FILES = 1024;

    /*******************************************************************************************************************
     * Guarded singleton constructor.
     ******************************************************************************************************************/
    FileTable();

    /*******************************************************************************************************************
     * Calculates the 10-bit hash of the specified file pointer as the file's table index.
     *
     * @param file File point whose table index we desire
     * @returns Table index for the file
     ******************************************************************************************************************/
    uint16_t hash(const size_t file) const;

    /** @see public:is_protected_file */
    bool is_protected_file(const size_t file, const bool is_throw) const;

    /*** @see public:track_file */
    void track_file(const size_t file, const bool is_protected_file);

    /** @see public:untrack_file */
    void untrack_file(const size_t file);

    /*******************************************************************************************************************
     * Record for a tracked file.
     ******************************************************************************************************************/
    struct FileTableEntry {
        /***************************************************************************************************************
         * The file being tracked
         **************************************************************************************************************/
        size_t file;

        /***************************************************************************************************************
         * Whether or not the file is protected
         **************************************************************************************************************/
        bool is_protected_file;
    };

    /*******************************************************************************************************************
     * This Singleton instance
     ******************************************************************************************************************/
    static FileTable* instance_;

    /*******************************************************************************************************************
     * List of entries for tracked files. If an entries file pointer is NULL, the record is empty.
     ******************************************************************************************************************/
    static FileTableEntry entries_[MAX_TRACKED_FILES];
}; // class FileTable

} // namespace evil_corp

#endif