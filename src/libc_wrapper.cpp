/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Core wrapping functionality
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

#include "libc_wrapper.h"

#include <dlfcn.h>
#include <cstdio>
#include <cstring>
#include <errno.h>
#include <syslog.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h> // NOTE(mmccann) - Including unistd seems to negatively affect out ability to inject?? https://stackoverflow.com/questions/17700712/ld-preload-and-clone
#include <sched.h>
#include <pthread.h>
#include <stdexcept>

#include "file_table.h"
#include "location_status.h"
#include "pid.h"
#include "protected_file.h"
#include "user_agent.h"

using namespace evil_corp;

extern "C" {

int access(const char *pathname, int mode) {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();
    int (*wrapped_access) (const char* pathname, int mode);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_access = (int (*) (const char* pathname, int mode)) dlsym(RTLD_NEXT, "access");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_access == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: access(%s, %d) = !DLERROR!\n", pid, tid, pathname, mode);
        return -1;
    }

    // Call the system function
    int return_value = wrapped_access(pathname, mode);

    // If this is a simple file existence check, or the access is already being denied
    if ((mode == F_OK) || (return_value != 0)) {
        fprintf(stderr, "[%d|%d] human-proof:: access(%s, %d) = %d\n", pid, tid, pathname, mode, return_value);

        // Go ahead and return the results - nothing tricky needs to happen
        return return_value;
    }

    // If we are within location context
    if (LocationStatus::get().is_within_location_context()) {
        fprintf(stderr, "[%d|%d] human-proof:: access(%s, %d) = %d (in-context)\n", pid, tid, pathname, mode, return_value);

        // Go ahead and return the results - nothing tricky needs to happen
        return return_value;
    }

    // If we have read access to the file
    if (wrapped_access(pathname, R_OK) == 0) {
        // If this is a protected file
        if (protected_file::check_is_protected_file_and_close(pathname)) {
            // Report this file as inaccessible, and set the errno accordingly
            return_value = -1;
            errno = EACCES;

            fprintf(stderr, "[%d|%d] human-proof:: access(%s, %d) = %d (protected|out-context)\n", pid, tid, pathname, mode, return_value);
        } else {
            fprintf(stderr, "[%d|%d] human-proof:: access(%s, %d) = %d (unprotected|out-context)\n", pid, tid, pathname, mode, return_value);
        }
    }

    fprintf(stderr, "[%d|%d] human-proof:: access(%s, %d) = %d (?|out-context)\n", pid, tid, pathname, mode, return_value);

    return return_value;
}

void apply_location_context_to_stat(const char* path, struct stat* buf) {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();
    int (*wrapped_access) (const char* pathname, int mode);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_access = (int (*) (const char* pathname, int mode)) dlsym(RTLD_NEXT, "access");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_access == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: apply_location_context_to_stat(%s, %p) = !DLERROR!\n", pid, tid, path, buf);
        return;
    }

    // If we are within location context
    if (LocationStatus::get().is_within_location_context()) {
        fprintf(stderr, "[%d|%d] human-proof:: apply_location_context_to_stat(%s, %p) = (in-context)\n", pid, tid, path, buf);

        // Go ahead and return the results - nothing tricky needs to happen
        return;
    }

    // If we have read access to the file
    if (wrapped_access(path, R_OK) == 0) {
        // If this is a protected file
        if (protected_file::check_is_protected_file_and_close(path)) {
            // Report this file as inaccessible
            buf->st_mode &= ~(S_IRWXU | S_IRWXG | S_IRWXO);

            fprintf(stderr, "[%d|%d] human-proof:: apply_location_context_to_stat(%s, %p) = (protected|out-context)\n", pid, tid, path, buf);
        } else {
            fprintf(stderr, "[%d|%d] human-proof:: apply_location_context_to_stat(%s, %p) = (unprotected|out-context)\n", pid, tid, path, buf);
        }
    }
}

int close(int fd) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_close) (int fd);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_close = (int (*) (int fd)) dlsym(RTLD_NEXT, "close");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_close == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: close(%d) = !DLERROR!]\n", pid, tid, fd);
        return -1;
    }

    // Clean up the file entry in the file table
    FileTable::get().untrack_file(fd);

    // Call the system function
    int return_value = wrapped_close(fd);
    fprintf(stderr, "[%d|%d] human-proof:: close(%d) = %d\n", pid, tid, fd, return_value);

    return return_value;
}

int dup(int oldfd) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_dup) (int oldfd);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_dup = (int (*) (int oldfd)) dlsym(RTLD_NEXT, "dup");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_dup == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: dup(%d) = !DLERROR!\n", pid, tid, oldfd);
        return -1;
    }

    // Call the system function
    int return_value = wrapped_dup(oldfd);
    fprintf(stderr, "[%d|%d] human-proof:: dup(%d) = %d\n", pid, tid, oldfd, return_value);

    FileTable::get().track_file(return_value, FileTable::get().is_protected_file(oldfd));

    return return_value;
}

int dup2(int oldfd, int newfd) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_dup2) (int oldfd, int newfd);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_dup2 = (int (*) (int oldfd, int newfd)) dlsym(RTLD_NEXT, "dup2");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_dup2 == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: dup2(%d, %d) = !DLERROR!\n", pid, tid, oldfd, newfd);
        return -1;
    }

    // Call the system function
    int return_value = wrapped_dup2(oldfd, newfd);
    fprintf(stderr, "[%d|%d] human-proof:: dup2(%d, %d) = %d\n", pid, tid, oldfd, newfd, return_value);

    FileTable::get().track_file(newfd, FileTable::get().is_protected_file(oldfd));

    return return_value;
}

int dup3(int oldfd, int newfd, int flags) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_dup3) (int oldfd, int newfd, int flags);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_dup3 = (int (*) (int oldfd, int inewfd, int flags)) dlsym(RTLD_NEXT, "dup3");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_dup3 == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: dup3(%d, %d, %d) = !DLERROR!\n", pid, tid, oldfd, newfd, flags);
        return -1;
    }

    // Call the system function
    int return_value = wrapped_dup3(oldfd, newfd, flags);
    fprintf(stderr, "[%d|%d] human-proof:: dup3(%d, %d, %d) = %d\n", pid, tid, oldfd, newfd, flags, return_value);

    FileTable::get().track_file(newfd, FileTable::get().is_protected_file(oldfd));

    return return_value;
}

int euidaccess(const char *pathname, int mode) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_access) (const char* pathname, int mode);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_access = (int (*) (const char* pathname, int mode)) dlsym(RTLD_NEXT, "euidaccess");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_access == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: euidaccess(%s, %d) = !DLERROR!\n", pid, tid, pathname, mode);
        return -1;
    }

    // Call the system function
    int return_value = wrapped_access(pathname, mode);

    if ((strcmp(pathname, "/tmp/test") == 0) || (strcmp(pathname, "/tmp/test") == 0)) {
        // If we are within the location context
        if (LocationStatus::get().is_within_location_context()) {
            fprintf(stderr, "[%d|%d] human-proof:: euidaccess(%s, %d) = %d (in-context)\n", pid, tid, pathname, mode, return_value);
        } else {
            if (mode != F_OK) {
                return_value = -1;
            }
            fprintf(stderr, "[%d|%d] human-proof:: euidaccess(%s, %d) = %d (out-context)\n", pid, tid, pathname, mode, return_value);
        }
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: euidaccess(%s, %d) = %d\n", pid, tid, pathname, mode, return_value);
    }

    return return_value;
}

int eaccess(const char *pathname, int mode) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_access) (const char* pathname, int mode);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_access = (int (*) (const char* pathname, int mode)) dlsym(RTLD_NEXT, "eaccess");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_access == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: euidaccess(%s, %d) = !DLERROR!\n", pid, tid, pathname, mode);
        return -1;
    }

    // Call the system function
    int return_value = wrapped_access(pathname, mode);

    if ((strcmp(pathname, "/tmp/test") == 0) || (strcmp(pathname, "/tmp/test") == 0)) {
        // If we are within the location context
        if (LocationStatus::get().is_within_location_context()) {
            fprintf(stderr, "[%d|%d] human-proof:: eaccess(%s, %d) = %d (in-context)\n", pid, tid, pathname, mode, return_value);
        } else {
            if (mode != F_OK) {
                return_value = -1;
            }
            fprintf(stderr, "[%d|%d] human-proof:: eaccess(%s, %d) = %d (out-context)\n", pid, tid, pathname, mode, return_value);
        }
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: eaccess(%s, %d) = %d\n", pid, tid, pathname, mode, return_value);
    }

    return return_value;
}

pid_t fork(void) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_fork) (void);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_fork = (int (*) (void)) dlsym(RTLD_NEXT, "fork");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_fork == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: fork() = !DLERROR!\n", pid, tid);
        return -1;
    }

    // Call the system function
    pid_t return_value = wrapped_fork();
    fprintf(stderr, "[%d|%d] human-proof:: fork() = %d\n", pid, tid, return_value);
    FileTable::get().print();

    return return_value;
}

int stat(const char* path, struct stat* buf) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_stat) (const char* path, struct stat* buf);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_stat = (int (*) (const char* path, struct stat* buf)) dlsym(RTLD_NEXT, "stat");

    // If a dynamic link error occurred
    if (dlerror()) {
        fprintf(stderr, "[%d|%d] human-proof:: stat(%s, %p) = !DLERROR!\n", pid, tid, path, buf);
        return -1;
    } else if (wrapped_stat == NULL) {
        fprintf(stderr, "[%d|%d] human-proof:: stat(%s, %p) = !NULL!\n", pid, tid, path, buf);
        return -1;
    }

    // Call the system function
    int result = wrapped_stat(path, buf);

    // If we are within the location context
    if (LocationStatus::get().is_within_location_context()) {
        fprintf(stderr, "[%d|%d] human-proof:: stat(%s, %p) = %d (in-context)\n", pid, tid, path, buf, result);
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: stat(%s, %p) = %d (out-context)\n", pid, tid, path, buf, result);
    //
    //    // Report this file as inaccessible
    //    buf->st_mode &= ~(S_IRWXU | S_IRWXG | S_IRWXO);
    }

    return result;
}

FILE *fdopen (int __fd, const char *__modes)  {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    FILE* (*wrapped_fdopen) (int __fd, const char* __modes);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_fdopen = (FILE * (*) (int __fd, const char* __modes)) dlsym(RTLD_NEXT, "fdopen");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_fdopen == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: fdopen(%d, %s) = !DLERROR!\n", pid, tid, __fd, __modes);
        return NULL;
    }

    // Call the system function
    FILE* file = wrapped_fdopen(__fd, __modes);

    // If the file was successfully opened
    if (file) {
        // Check if this is a protected file
        bool is_protected_file = protected_file::check_is_protected_file_on_open(file);
        if (is_protected_file) {
            // If we are within the location context
            if (LocationStatus::get().is_within_location_context()) {
                fprintf(stderr, "[%d|%d] human-proof:: fdopen(%d, %s) = %p (protected|in-context)\n", pid, tid, __fd, __modes, file);
            } else {
                fprintf(stderr, "[%d|%d] human-proof:: fdopen(%d, %s) = %p (protected|out-context)\n", pid, tid, __fd, __modes, file);
            }

        } else {
            fprintf(stderr, "[%d|%d] human-proof:: fdopen(%d, %s) = %p (unprotected)\n", pid, tid, __fd, __modes, file);
        }

        // Add this file to the table of opened files
        FileTable::get().track_file(file, is_protected_file);
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: fdopen(%d, %s) = NULL\n", pid, tid, __fd, __modes);
    }

    return file;
}

int __IO_getc(FILE* stream) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_getc) (FILE* stream);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_getc = (int (*) (FILE*)) dlsym(RTLD_NEXT, "getc");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_getc == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: __IO_getc(%p) = !DLERROR!\n", pid, tid, stream);
        return EOF;
    }

    // Call the system function
    int return_value = wrapped_getc(stream);

    // If we are at the end of the stream
    if (return_value == EOF) {
        fprintf(stderr, "[%d|%d] human-proof:: __IO_getc(%p) = EOF\n", pid, tid, stream);
    } else {
        // If this is a protected file
        if (FileTable::get().is_protected_file(stream)) {
            return_value ^= protected_file::CIPHER_MASK;

            fprintf(stderr, "[%d|%d] human-proof:: __IO_getc(%p) = %c (protected)\n", pid, tid, stream, return_value);
        } else {
            fprintf(stderr, "[%d|%d] human-proof:: __IO_getc(%p) = %c (unprotected)\n", pid, tid, stream, return_value);
        }
    }

    return return_value;
}

void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    void* (*wrapper_mmap) (void *addr, size_t length, int prot, int flags, int fd, off_t offset);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapper_mmap = (void* (*) (void *addr, size_t length, int prot, int flags, int fd, off_t offset)) dlsym(RTLD_NEXT, "mmap");

    // If a dynamic link error occurred
    if (dlerror() || (wrapper_mmap == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: mmap(%p, %lu, %d, %d, %d, %lu) = !DLERROR!\n", pid, tid, addr, length, prot, flags, fd, offset);
        return NULL;
    }

    // Call the system function
    void* final_addr = wrapper_mmap(addr, length, prot, flags, fd, offset);

    // If this is a protected file and the memory was allocated successfully
    if (final_addr && FileTable::get().is_protected_file(fd)) {
        for (size_t iter = 0; iter < length; iter++) {
            ((char*) final_addr)[iter] ^= protected_file::CIPHER_MASK;
        }
        fprintf(stderr, "[%d|%d] human-proof:: mmap(%p, %lu, %d, %d, %d, %lu) = %p (protected)\n", pid, tid, addr, length, prot, flags, fd, offset, final_addr);
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: mmap(%p, %lu, %d, %d, %d, %lu) = %p (unprotected)\n", pid, tid, addr, length, prot, flags, fd, offset, final_addr);
    }

    return final_addr;
}

int open(const char *path, int oflag, ... ) {
    uint16_t pid = my_getpid();
    uint16_t tid = 0;//uint16_t tid = my_gettid();
    int (*wrapped_open) (const char* path, int mode) = NULL;

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_open = (int (*) (const char*, int)) dlsym(RTLD_NEXT, "open");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_open == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: open(%s, %d) = !DLERROR!\n", pid, tid, path, oflag);
        return 0;
    }

    // Call the system function
    int file = wrapped_open(path, oflag);

    // If the file was successfully opened
    if (file != -1) {
        // Check if this is a protected file
        bool is_protected_file = protected_file::check_is_protected_file_on_open(file);
        if (is_protected_file) {
            /*// If we are within the location context
            if (LocationStatus::get().is_within_location_context()) {
                fprintf(stderr, "[%d|%d] human-proof:: open(%s, %d) = %d (protected|in-context)\n", pid, tid, path, oflag, file);
            } else {
                file = -1;
                errno = EACCES;
                fprintf(stderr, "[%d|%d] human-proof:: open(%s, %d) = %d (protected|out-context)\n", pid, tid, path, oflag, file);
                return file;
            }*/

            // Wait for access approval
            UserAgent::get().check_file_access();
        } else {
            fprintf(stderr, "[%d|%d] human-proof:: open(%s, %d) = %d (unprotected)\n", pid, tid, path, oflag, file);
        }

        // Add this file to the table of opened files
        FileTable::get().track_file(file, is_protected_file);
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: open(%s, %d) = -1\n", pid, tid, path, oflag);
    }

    return file;
}

ssize_t pread(int fd, void *buf, size_t count, off_t offset) {\
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    bool is_protected_file;
    ssize_t (*wrapped_pread) (int df, void* buf, size_t count, off_t offset);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_pread = (ssize_t (*) (int fd, void* buf, size_t count, off_t offset)) dlsym(RTLD_NEXT, "pread");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_pread == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: pread(%d, %p, %lu, %lu) = !DLERROR!\n", pid, tid, fd, buf, count, offset);
        return EOF;
    }

    // Check if this is a protected file
    is_protected_file = FileTable::get().is_protected_file(fd);

    // If this is a protected file, adjust the offset so that we present offset 0 as the first byte after the preamble
    if (is_protected_file) {
        offset += 64;
    }

    // Call the system function
    size_t bytes_read = wrapped_pread(fd, buf, count, offset);

    // If this is a protected file
    if (is_protected_file) {
        fprintf(stderr, "[%d|%d] human-proof:: pread(%d, %p, %lu, %lu) = %lu (protected)\n", pid, tid, fd, buf, count, offset, bytes_read);

        for (size_t iter = 0; iter < bytes_read; iter++) {
            ((char*) buf)[iter] ^= protected_file::CIPHER_MASK;
        }
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: pread(%d, %p, %lu, %lu) = %lu (unprotected)\n", pid, tid, fd, buf, count, offset, bytes_read);
    }



    return bytes_read;
}

int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg) {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();

    ssize_t (*wrapped_pthread_create) (pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_pthread_create = (ssize_t (*) (pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg)) dlsym(RTLD_NEXT, "pthread_create");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_pthread_create == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: pthread_create(...) = !DLERROR!\n", pid, tid);
        return EOF;
    }

    // Call the system function
    int return_value = wrapped_pthread_create(thread, attr, start_routine, arg);

    fprintf(stderr, "[%d|%d] human-proof:: pthread_create(%p, ...) = %d\n", pid, tid, *thread, return_value);

    return return_value;
}

ssize_t read(int fd, void *buf, size_t count) {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();
    ssize_t (*wrapped_read) (int df, void* buf, size_t count);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_read = (ssize_t (*) (int df, void* buf, size_t count)) dlsym(RTLD_NEXT, "read");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_read == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: read(%d, %p, %lu) = !DLERROR!\n", pid, tid, fd, buf, count);
        return EOF;
    }

    // Call the system function
    size_t bytes_read = wrapped_read(fd, buf, count);

    FileTable::get().print();

    // Check if we can look up the protected status from the file table (i.e. intra-process i/o)
    bool is_protected_file = false;
    try {
        is_protected_file = FileTable::get().is_protected_file__throw(fd);
    // This file descriptor isn't tracked - Maybe this is an inter-process i/o, let's try to recover!
    } catch (const std::runtime_error& ex) {
        // Calculate the out the proc location describing this file descriptor
        char proc_path[1024];
        sprintf(proc_path, "/proc/self/fd/%d", fd);

        // Try to lookup the file path attached to the file descriptor
        char file_path[1024];
        readlink(proc_path, file_path, 1024);

        // If this is a valid file path (and not a socket path)
        if (file_path[0] == '/') {
            // Reopen the file and check if it is protected
            is_protected_file = protected_file::check_is_protected_file_and_close(file_path);

            // Track the protection state so we don't have to do this again
            FileTable::get().track_file(fd, is_protected_file);
        }
    }

    // If this is a protected file
    if (is_protected_file) {
        fprintf(stderr, "[%d|%d] human-proof:: read(%d, %p, %lu) = %lu (protected)\n", pid, tid, fd, buf, count, bytes_read);

        for (size_t iter = 0; iter < bytes_read; iter++) {
            ((char*) buf)[iter] ^= protected_file::CIPHER_MASK;
        }
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: read(%d, %p, %lu) = %lu (unprotected)\n", pid, tid, fd, buf, count, bytes_read);
    }

    return bytes_read;
}

/*int socket(int domain, int type, int protocol) {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();
    int (*wrapped_socket) (int domain, int type, int protocol);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_socket = (int (*) (int, int, int)) dlsym(RTLD_NEXT, "socket");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_socket == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: socket(%d, %d, %d) = !DLERROR!\n", pid, tid, domain, type, protocol);
        return -1;
    }

    // Call the system function
    int return_value = wrapped_socket(domain, type, protocol);
    fprintf(stderr, "[%d|%d] human-proof:: socket(%d, %d, %d) = %d\n", pid, tid, domain, type, protocol, return_value);

    char bufin[1024];
    char bufout[1024];
    sprintf(bufin, "/proc/self/fd/%d", return_value);
    readlink(bufin, bufout, 1024);
    fprintf(stderr, "PATH: %s\n", bufout);

    return return_value;
}*/

pid_t vfork(void) {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();
    int (*wrapped_vfork) (void);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_vfork = (int (*) (void)) dlsym(RTLD_NEXT, "vfork");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_vfork == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: vfork() = !DLERROR!\n", pid, tid);
        return -1;
    }

    // Call the system function
    pid_t return_value = wrapped_vfork();
    fprintf(stderr, "[%d|%d] human-proof:: vfork() = %d\n", pid, tid, return_value);

    return return_value;
}

int __xstat(int ver, const char *path, struct stat *buf) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_xstat) (int ver, const char* path, struct stat* buf);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_xstat = (int (*) (int ver, const char* path, struct stat* buf)) dlsym(RTLD_NEXT, "__xstat");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_xstat == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: __xstat(%d, %s, %p) = !DLERROR!\n", pid, tid, ver, path, buf);
        return -1;
    }

    // Call the system function
    int result = wrapped_xstat(ver, path, buf);

    // If the system function call didn't error out
    if (result == 0) {
        // Update the stat structure if needed
        apply_location_context_to_stat(path, buf);
    }

    fprintf(stderr, "[%d|%d] human-proof:: __xstat(%d, %s, %p) = %d\n", pid, tid, ver, path, buf, result);

    return result;
}

int __fxstat(int ver, int fd, struct stat *buf) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_fxstat) (int ver, int fd, struct stat* buf);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_fxstat = (int (*) (int ver, int fd, struct stat* buf)) dlsym(RTLD_NEXT, "__fxstat");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_fxstat == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: __fxstat(%d, %d, %p) = !DLERROR!\n", pid, tid, ver, fd, buf);
        return -1;
    }

    // Call the system function
    int result = wrapped_fxstat(ver, fd, buf);

    // If we are within the location context
    if (LocationStatus::get().is_within_location_context()) {
        fprintf(stderr, "[%d|%d] human-proof:: __fxstat(%d, %d, %p) = %d (in-context)\n", pid, tid, ver, fd, buf, result);
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: __fxstat(%d, %d, %p) = %d (out-context)\n", pid, tid, ver, fd, buf, result);

        //fprintf(stderr, "[%d|%d] S_IRUSR = %d", pid, tid, buf->st_mode & S_IRUSR);

        // Report this file as inaccessible
        //buf->st_mode &= ~(S_IRWXU | S_IRWXG | S_IRWXO);

        //fprintf(stderr, "[%d|%d] S_IRUSR = %d", pid, tid, buf->st_mode & S_IRUSR);
    }

    return result;
}

int __lxstat(int ver, const char *path, struct stat *buf) {
    uint16_t pid = my_getpid();uint16_t tid = my_gettid();
    int (*wrapped_lxstat) (int ver, const char* path, struct stat* buf);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_lxstat = (int (*) (int ver, const char* path, struct stat* buf)) dlsym(RTLD_NEXT, "__lxstat");

    // If a dynamic link error occurred
    if (dlerror() || (wrapped_lxstat == NULL)) {
        fprintf(stderr, "[%d|%d] human-proof:: __lxstat(%d, %s, %p) = !DLERROR!\n", pid, tid, ver, path, buf);
        return -1;
    }

    // Call the system function
    int result = wrapped_lxstat(ver, path, buf);

    // If the system function call didn't error out
    if (result == 0) {
        // Update the stat structure if needed
        apply_location_context_to_stat(path, buf);
    }

    fprintf(stderr, "[%d|%d] human-proof:: __lxstat(%d, %s, %p) = %d\n", pid, tid, ver, path, buf, result);

    return result;
}

//int clone(int (*fn)(void *), void *child_stack,
//                 int flags, void *arg, ...
//             /* pid_t *ptid, void *newtls, pid_t *ctid */ ) {
//    uint16_t pid = my_getpid();
//    uint16_t tid = my_gettid();
//    fprintf(stderr, "[%d|%d] clone!", pid, tid);
//    return -1;
//}

/*int fstat(int fd, struct stat *buf) {
    int (*wrapped_fstat) (int fd, struct stat* buf);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the wrapped function
    wrapped_fstat = (int (*) (int path, struct stat* buf)) dlsym(RTLD_NEXT, "fstat");

    // If a dynamic link error occurred
    if (dlerror()) {
        fprintf(stderr, "[%d|%d] human-proof:: fstat(%d, %p) = !DLERROR!", pid, tid, fd, buf);
        return -1;
    }

    // Call the system function
    int result = wrapped_fstat(fd, buf);

    // If we are within the location context
    if (LocationStatus::get().is_within_location_context()) {
        fprintf(stderr, "[%d|%d] human-proof:: fstat(%d, %p) = %d (in-context)", pid, tid, fd, buf, result);
    } else {
        fprintf(stderr, "[%d|%d] human-proof:: fstat(%d, %p) = %d (out-context)", pid, tid, fd, buf, result);

        // Report this file as inaccessible
        buf->st_mode &= ~(S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
    }

    return result;
}*/

} // extern "C"
