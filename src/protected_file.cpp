#include "protected_file.h"

#include <cstring>
#include <dlfcn.h>
#include <fcntl.h>
#include <stdexcept>
#include <sys/types.h>
#include <unistd.h>

#include "pid.h"

using namespace evil_corp;

bool protected_file::check_is_protected_file_and_close(const char* pathname) {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();
    int file;
    bool is_protected_file = false;
    int (*fclose) (FILE *stream);
    FILE* (*fopen) (const char *path, const char* mode);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the fopen function
    fopen = (FILE* (*) (const char *path, const char* mode)) dlsym(RTLD_NEXT, "fopen");

    // If a dynamic link error occurred
    if (dlerror()) {
        fprintf(stderr, "[%d|%d] human-proof:: check_is_protected_file_and_close(%s) = !fopen = DLERROR\n", pid, tid, pathname);
        return false;
    }

    // If the open function wasn't fetched
    if (fopen == NULL) {
        fprintf(stderr, "[%d|%d] human-proof:: check_is_protected_file_and_close(%s) = !fopen = NULL!\n", pid, tid, pathname);
        return false;
    }
    // Fetch the fclose function
    fclose = (int (*) (FILE*)) dlsym(RTLD_NEXT, "fclose");

    // If a dynamic link error occurred
    if (dlerror()) {
        fprintf(stderr, "[%d|%d] human-proof:: check_is_protected_file_and_close(%s) = !fclose = DLERROR\n", pid, tid, pathname);
        return false;
    }

    // If the fclose function wasn't fetched
    if (fclose == NULL) {
        fprintf(stderr, "[%d|%d] human-proof:: check_is_protected_file_and_close(%s) = !fclose = NULL!\n", pid, tid, pathname);
        return false;
    }

    int (*wrapped_open) (const char*, int) = (int (*) (const char*, int)) dlsym(RTLD_NEXT, "open");

    // Open the file
    file = wrapped_open(pathname, O_RDONLY);
    fprintf(stderr, "[%d|%d] WOOF: %d, %d\n", pid, tid, file, errno);

    // Check if the file is protected
    is_protected_file = protected_file::check_is_protected_file_on_open(file);

    // Clean up the file // TODO Error checking
    close(file);

    return is_protected_file;
}

bool protected_file::check_is_protected_file_on_open(FILE* const file) {
    uint8_t preamble[PREAMBLE_SIZE];
    size_t (*wrapped_fread) (void* ptr, size_t size, size_t count, FILE* stream);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the original fread function
    wrapped_fread = (size_t (*) (void*, size_t, size_t, FILE*)) dlsym(RTLD_NEXT, "fread");

    // If a dynamic link error occurred
    if (dlerror()) {
        fprintf(stderr, "human-proof:: check_is_protected_file_on_open(%p) = !DLERROR!", file);
        return false;
    }

    // Read in the first len(preamble) bytes
    wrapped_fread(preamble, sizeof(uint8_t), PREAMBLE_SIZE, file);

    // Check if the preamble is present
    bool is_preamble_present = true;
    for (size_t iter = 0; iter < PREAMBLE_SIZE; iter++) {
        if (preamble[iter] != PREAMBLE[iter]) {
            is_preamble_present = false;
            break;
        }
    }

    // If the bytes are the protected file preamble
    if (is_preamble_present) {
        return true;
    }

    // Rewind to the beginning of the file
    fseek(file, 0, SEEK_SET);

    return false;
}

bool protected_file::check_is_protected_file_on_open(const int file) {
    uint16_t pid = my_getpid();
    uint16_t tid = my_gettid();
    fprintf(stderr, "[%d|%d] fd check\n", pid, tid);
    uint8_t preamble[PREAMBLE_SIZE];
    size_t (*wrapped_pread) (int fd, void *buf, size_t count, off_t offset);

    // Clear any existing dlerror message
    dlerror();

    // Fetch the original fread function
    wrapped_pread = (size_t (*) (int fd, void *buf, size_t count, off_t offset)) dlsym(RTLD_NEXT, "pread");

    // If a dynamic link error occurred
    if (dlerror()) {
        fprintf(stderr, "human-proof:: check_is_protected_file_on_open(%i) = !DLERROR!", file);
        return false;
    }

    // Read in the first len(preamble) bytes
    wrapped_pread(file, preamble, PREAMBLE_SIZE, 0);

    // Check if the preamble is present
    bool is_preamble_present = true;
    for (size_t iter = 0; iter < PREAMBLE_SIZE; iter++) {
        if (preamble[iter] != PREAMBLE[iter]) {
            is_preamble_present = false;
            break;
        }
    }

    // If the bytes are the protected file preamble
    if (is_preamble_present) {
        // Rewind to the beginning of the file
        lseek(file, PREAMBLE_SIZE, SEEK_SET);

        return true;
    }

    return false;
}

void protected_file::to_ciphertext(uint8_t* plaintext_buffer, uint8_t* ciphertext_buffer, const size_t buffer_length) {
    // Copy the plaintext over so we don't overwrite memory owned by the userspace application
    memcpy(plaintext_buffer, ciphertext_buffer, buffer_length);

    // Apply the symmetric "encryption"
    for (size_t iter = 0; iter < buffer_length; iter++) {
        ciphertext_buffer[iter] ^= CIPHER_MASK;
    }
}

void protected_file::to_plaintext(uint8_t* buffer, const size_t buffer_length) {
    // Apply the symmetric "decryption"
    for (size_t iter = 0; iter < buffer_length; iter++) {
        buffer[iter] ^= CIPHER_MASK;
    }
}