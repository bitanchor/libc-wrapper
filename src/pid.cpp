#include "pid.h"

#include <unistd.h>
#include <sys/syscall.h>

extern "C" {

uint16_t my_getpid() {
    return (uint16_t) getpid();
}

uint16_t my_gettid() {
    return (uint16_t) syscall(SYS_gettid);
}

}