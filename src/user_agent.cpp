#include "user_agent.h"

#include <dlfcn.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sstream>
#include <unistd.h>

#include "pid.h"

using namespace evil_corp;
using namespace std;

UserAgent* UserAgent::instance_ = NULL;

UserAgent& UserAgent::get() {
    // If this singleton has not yet been initialized
    if (instance_ == NULL) {
        // Initialize the singleton
        instance_ = new UserAgent();
        instance_->init();
    }

    return *instance_;
}

void UserAgent::init() {
    // Nothing so far
}

bool UserAgent::check_file_access() const {
    int (*wrapped_access) (const char* pathname, int mode) = (int (*) (const char* pathname, int mode)) dlsym(RTLD_NEXT, "access");
    int (*wrapped_open) (const char* path, int mode) = (int (*) (const char*, int)) dlsym(RTLD_NEXT, "open");

    // Open the request pipe
    int request_pipe = wrapped_open("/tmp/human-proof-file-access", O_WRONLY);

    // Write out the file access request
    stringstream request;
    request << my_getpid() << " 0\n";
    write(request_pipe, request.str().c_str(), request.str().size());

    // Wait for the access approved touch to appear
    stringstream approved_path;
    approved_path << "/tmp/approved." << my_getpid();
    while (0 != wrapped_access(approved_path.str().c_str(), 0)) {
        usleep(1000);
    }

    return true;
}
