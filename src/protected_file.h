/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Holds basic protected file operations
 * @copyright &copy; 2017 R. Matt McCann
 **********************************************************************************************************************/

#ifndef LIBC_WRAPPER__PROTECTED_FILE__H_
#define LIBC_WRAPPER__PROTECTED_FILE__H_

#include <cstdint>
#include <cstdio>

namespace evil_corp {

namespace protected_file {
    /*******************************************************************************************************************
     * The symmetric "encyption" mask applied to protected file data.
     ******************************************************************************************************************/
    static constexpr uint8_t CIPHER_MASK = 0b1010;

    /*******************************************************************************************************************
     * Preamble prepended to protected files to make them quickly identifiable upon open.
     ******************************************************************************************************************/
    static const size_t PREAMBLE_SIZE = 64;
    static constexpr uint8_t PREAMBLE[PREAMBLE_SIZE+1] =
        "\x62\x8c\xb5\x06\xb5\x9f\xb8\xdd\x96\xa6\xa5\xa1\x89\xd6\xb8\xff\xcb\xcc\xb1\xac\x8a\x03"
        "\xda\xb8\xdd\x96\xa6\xa5\xc5\x1b\xd8\x9e\x13\xbf\xf7\xc8\xc4\x81\xce\xd1\xa9\x0c\x04\xb7"
        "\x14\xf2\xda\x1e\x12\xa4\xdd\xb8\xdd\x96\xa6\xa5\x89\xd6\xb8\xff\xcb\xcc\xb1\x30";

    /*******************************************************************************************************************
     * Checks for the presence of an evil corp preamble to indicate if the file is protected; Cleans up the file
     * after completing the check.
     *
     * @param pathname Path of the file to check
     * @returns True if the file is protected
     ******************************************************************************************************************/
    bool check_is_protected_file_and_close(const char* pathname);

    /*******************************************************************************************************************
     * Checks for the presence of an evil corp preamble to indicate the file is protected. Should be called on open!
     *
     * @param file The file to be checked for protected status
     * @returns True if the file is protected
     ******************************************************************************************************************/
    bool check_is_protected_file_on_open(FILE* const file);
    bool check_is_protected_file_on_open(const int file);

    /*******************************************************************************************************************
     * Converts the plaintext buffer into ciphertext.
     *
     * @param plaintext_buffer Plaintext buffer to be converted.
     * @param ciphertext_buffer Buffer to hold the converted ciphertext
     * @param buffer_length Length of the plaintext buffer
     ******************************************************************************************************************/
    void to_ciphertext(uint8_t* plaintext_buffer, uint8_t* ciphertext_buffer, const size_t buffer_length);

    /*******************************************************************************************************************
     * Converts the ciphertext buffer into plaintext.
     *
     * @param buffer Ciphertext buffer to be converted.
     * @param buffer_length Length of the ciphertext buffer
     ******************************************************************************************************************/
    void to_plaintext(uint8_t* buffer, const size_t buffer_length);

}; // namespace ProtectedFile

}; // namespace evil_corp

#endif