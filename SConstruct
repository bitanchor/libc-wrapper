'''
@blame R. Matt McCann <mccann.matt@gmail.com>
@brief Script to drive build script
@copyright &copy; 2017 Evil Corp.
'''

import os

import misc.build_helper as build_helper

from misc.build_helper import collect_sources, configure_scons_shared_library

# TODO(mmccann) - Fix the SCons package availability to pylint behaves usefully in the build scripts
# from SCons.Environment import SConscript

# If this is not being ran inside of a Docker container
if not os.path.exists('/.dockerenv'):
    print("You are not running scons inside of the developer Docker container - this is not recommended!")

# Setup the project-wide build environment settings
build_helper.env = Environment() # noqa

# Initialize the libraries directory if needed
if not os.path.exists('lib'):
    os.makedirs('lib')

c_flags = ['-D_GNU_SOURCE', '-D_REENTRANT', '-std=gnu11', '-g']
cpp_flags = ['-D_GNU_SOURCE', '-D_REENTRANT', '-std=gnu++11', '-g']
libs = ['dl']
link_flags = ['-nostartfiles', '-fPIC', '-pthread']

# Collect a list of all source files in this directory
source_files = collect_sources('.')

# Fetch the build environment
build_helper.env.Append(CFLAGS=c_flags)
build_helper.env.Append(CXXFLAGS=cpp_flags)
build_helper.env.Append(LIBS=libs)
build_helper.env.Append(LINKFLAGS=link_flags)
build_helper.env.Replace(CC='gcc')
build_helper.env.Replace(CXX='g++')

# Configure the library generation
configure_scons_shared_library(target='#lib/libc_wrapper', source=source_files, env=build_helper.env)

#SConscript('#test/SConscript') # noqa
